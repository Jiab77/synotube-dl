#!/bin/bash

###########################################################################
# Simple script that will download YouTube videos from a Synology NAS     #
#                                                                         #
# Thanks to https://github.com/kwent/syno for the CLI used in this script #
#                                                                         #
# Made by Jiab77                                                          #
###########################################################################

# Colors
NC="\033[0m"
NL="\n"
RED="\033[1;31m"
GREEN="\033[1;32m"
YELLOW="\033[1;33m"
BLUE="\033[1;34m"
PURPLE="\033[1;35m"
CYAN="\033[1;36m"
WHITE="\033[1;37m"

# Header
echo -e "${NL}${BLUE}Simple script to download ${GREEN}YouTube${BLUE} videos to ${PURPLE}MP4${BLUE}...${NC}"

# Check arguments
[[ $# -eq 0 ]] && echo -e "${NL}${WHITE}Usage: ${YELLOW}$0 ${PURPLE}<url> [frame-offset]${NC}${NL}" && exit 0

# Config
URL=$1
START_OFFSET=$2
DEFAULT_OFFSET="00:00:01.00"
RESAMPLE=true

# Defining frame offset for gathering MP3 cover
if [[ ! $# -eq 2 ]]; then
    START_OFFSET=$DEFAULT_OFFSET
fi

# Process URL
echo -e "${NL}${WHITE}Creating download task for ${GREEN}${URL}${WHITE}...${NC}"
syno dl -i createTask --payload '{"uri":"'${URL}'"}'

# Show status
echo -e "${NL}${WHITE}Gathering task details...${NC}${NL}"
sleep 2
syno dl -i listTasks | jq .

# Get status
ID=$(syno dl -i listTasks | jq -r '.tasks[0].id')
STATUS=$(syno dl -i listTasks | jq -r '.tasks[0].status')
NAME=$(syno dl -i listTasks | jq -r '.tasks[0].title')
SIZE=$(syno dl -i listTasks | jq -r '.tasks[0].size')

# Check status
echo -e "${NL}${WHITE}Downloading ${GREEN}${NAME}${WHITE}...${NC}"
sleep 2
while true; do
    sleep 5
    STATUS=$(syno dl -i listTasks | jq -r '.tasks[0].status')
    echo -e "${WHITE}  - Status: ${GREEN}${STATUS}${NC}"
    if [[ $STATUS == "finished" ]]; then
        break
    fi
done
sleep 5
echo -e "${NL}${WHITE}File ${GREEN}${NAME}${WHITE} downloaded.${NC}"

# Process file
echo -e "${NL}${WHITE}Converting ${GREEN}${NAME}${WHITE} to ${PURPLE}MP3${WHITE}...${NC}${NL}"
if [[ $RESAMPLE == true ]]; then
    ffmpeg -y -hide_banner -threads 0 -i "${NAME}" -codec:a libmp3lame -q:a 0 -af aresample=48000 "${NAME}.mp3"
else
    ffmpeg -y -hide_banner -threads 0 -i "${NAME}" -codec:a libmp3lame -q:a 0 "${NAME}.mp3"
fi
echo -e "${NL}${WHITE}Extracting cover from ${GREEN}${NAME}${WHITE}...${NC}${NL}"
ffmpeg -y -hide_banner -threads 0 -ss $START_OFFSET -i "${NAME}" -map 0:v -vframes 1 "${NAME}.cover.jpg"
echo -e "${NL}${WHITE}Adding extracted cover ${GREEN}${NAME}.cover.jpg${WHITE} to ${PURPLE}${NAME}.mp3${WHITE}...${NC}${NL}"
ffmpeg -y -hide_banner -threads 0 -i "${NAME}.mp3" -i "${NAME}.cover.jpg" -map 0:0 -map 1:0 -c copy -id3v2_version 3 -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" "${NAME}.covered.mp3"
echo -e "${NL}${YELLOW}Removing created temp files...${NC}${NL}"
rm -fv "${NAME}.cover.jpg" "${NAME}.mp3"

# Remove task from the NAS
echo -e "${NL}${YELLOW}Removing download task from the NAS...${NC}${NL}"
syno dl -i deleteTask --payload '{"id":"'${ID}'"}' | jq .

# End
echo -e "${NL}${BLUE}Done.${NC}${NL}"
