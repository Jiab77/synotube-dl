# synotube-dl

Simple script that use Synology NAS's for downloading Youtube videos, convert them to MP3 and automatically adds metadatas and cover.

Tested with `Download Station` version:

* `3.8.15-3563`

Pending test with version:

* `3.8.16-3566`

## Security

A vulnerability allows remote authenticated users to execute arbitrary code via a susceptible version of Download Station.

Upgrade to `3.8.16-3566` will solve the issue.

See here for more details: https://www.synology.com/fr-fr/security/advisory/Synology_SA_21_11
